var stockquotes = window.stockquotes, io, data;

(function () {
    "use strict";

    window.stockquotes = {
        series: {},
        socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php"
        },

        // TESTED
        // parses data for use
        parseData: function (rows) {
            var i, company;

            // iterate over rows and add to series
            for (i = 0; i < rows.length; i += 1) {
                company = rows[i].col0;

                //check if array for company exists in series
                if (stockquotes.series[company] !== undefined) {
                    stockquotes.series[company].push(rows[i]);
                } else {
                    stockquotes.series[company] = [rows[i]];
                }
            }
        },

        // gets data using AJAX request, do not test
        retrieveData: function () {
            var xhr;

            xhr = new XMLHttpRequest();
            xhr.open("GET", stockquotes.settings.ajaxUrl, true);
            xhr.addEventListener("load", stockquotes.getJSONString);
            xhr.send();
        },

        // TESTED
        // gets data out of JSON string
        getJSONString: function (e) {
            var response = e.target.responseText, obj;

            obj = JSON.parse(response);
            stockquotes.parseData(obj.query.results.row);
        },

        // initializes the data and HTML
        init: function () {
            var container, bodyNode;

            bodyNode = document.querySelector("body");

            container = stockquotes.initHTML();
            bodyNode.appendChild(container);

            stockquotes.parseData(data.query.results.row); // shows initial test data, comment out when using sockets or AJAX
            container.appendChild(stockquotes.showData());

            //stockquotes.getRealTimeData(); // gets socket data, comment out when using testdata or AJAX
            stockquotes.loop(); // comment out when using sockets
        },

        // TESTED
        // initializes HTML
        initHTML: function () {
            var container, h1Node;

            container = document.createElement("div");
            container.id = "container";

            stockquotes.container = container;

            h1Node = document.createElement("h1");
            h1Node.innerText = "Real time Stockquotes app";

            stockquotes.container.appendChild(h1Node);

            return stockquotes.container;
        },

        // TESTED
        // fills table with data
        showData: function () {
            var company, table, header, row, cell, quote, propertyName, propertyValue;

            //create table
            table = document.createElement("table");

            //create header
            header =  table.createTHead();
            row = header.insertRow(0);
            cell = row.insertCell(0);
            cell.innerHTML = "Company";
            cell = row.insertCell(1);
            cell.innerHTML = "  Date";
            cell = row.insertCell(2);
            cell.innerHTML = "  Time";
            cell = row.insertCell(3);
            cell.innerHTML = "  Price";

            //create rows
            for (company in stockquotes.series) {
                if (stockquotes.series.hasOwnProperty(company)) {
                    quote = stockquotes.series[company][stockquotes.series[company].length - 1];
                    row = document.createElement("tr");
                    row.id = stockquotes.createValidCSSNameFromCompany(company);
                    stockquotes.color(quote.col4, row);

                    //create cells
                    table.appendChild(row);

                    //iterate over quote to create cells
                    for (propertyName in quote) {
                        if (quote.hasOwnProperty(propertyName)) {
                            propertyValue = quote[propertyName];
                            cell = document.createElement("td");
                            cell.innerText = propertyValue;
                            row.appendChild(cell);
                        }
                    }
                }
            }

            return table;
        },

        // TESTED
        // determines color of the row
        color: function (col, row) {
            if (col > 0) {
                row.className = "rise";
            } else if (col < 0) {
                row.className = "fall";
            } else {
                row.className = "neutral";
            }
        },

        // TESTED
        // removes special characters from company name
        createValidCSSNameFromCompany: function (str) {
            return str.replace(/[^a-zA-Z_0-9]/g, "");
        },

        // TESTED
        // returns a random float between min and max of current value
        rnd: function (min, max) {
            var returnvalue;

            returnvalue = Math.random() * (max - min + 1) + min;
            returnvalue = Math.round(returnvalue * 100) / 100;

            return returnvalue;
        },

        // TESTED
        // return formatted date string mm/dd/yyyy
        getFormattedDate: function (date) {
            var dateResult, month, day, year;

            month = parseInt(date.getMonth(), 10) + 1;
            day = parseInt(date.getDate(), 10);
            year = date.getFullYear().toString();

            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            dateResult = month + "/" + day + "/" + year;
            //console.log(datetime);

            return dateResult;
        },

        // TESTED
        // return formatted time am, pm
        getFormattedTime: function (date) {
            var time, hours, minutes;

            hours = date.getHours().toFixed();
            minutes = date.getMinutes().toString();

            if (hours.length > 1) {
                if (minutes.length < 2) {
                    minutes = "0" + minutes.toString();
                }
                time = (hours - 12).toString() + ":" + minutes + " pm";
            } else {
                time = hours.toString() + ":" + minutes + " am";
            }
            //console.log(time);

            return time;
        },

        // TESTED
        // generates random data
        generateTestData: function () {
            var company, quote, newQuote, date;
            date = new Date();

            for (company in stockquotes.series) {
                if (stockquotes.series.hasOwnProperty(company)) {
                    quote = stockquotes.series[company][0];
                    newQuote = Object.create(quote);
                    newQuote.col0 = company;
                    newQuote.col2 = stockquotes.getFormattedDate(date);
                    newQuote.col3 = stockquotes.getFormattedTime(date);
                    newQuote.col4 = stockquotes.rnd(-10, 10); //price difference between current and last value
                    newQuote.col5 = stockquotes.rnd(-10, 1000);
                    newQuote.col6 = stockquotes.rnd(-10, 1000);
                    newQuote.col7 = "N/A";
                    newQuote.col8 = "NaN";
                    stockquotes.series[company].push(newQuote);
                }
            }
            //console.log(stockquotes.series);
        },

        // gets data through socket, do not test
        getRealTimeData: function () {
            stockquotes.socket.on("stockquotes", function (data) {
                stockquotes.parseData(data.query.results.row);
                stockquotes.replaceTable();
            });
        },

        // TESTED
        loop: function () {
            stockquotes.generateTestData(); // gets test data, comment out when using AJAX
            //stockquotes.retrieveData(); // gets AJAX data, comment out when using testdata

            stockquotes.replaceTable();

            setTimeout(stockquotes.loop, stockquotes.settings.refresh);
        },

        // TESTED
        replaceTable: function () {
            var container;

            container = document.querySelector("#container");

            // remove old table
            document.querySelector("#container").removeChild(document.querySelector("table"));

            // add new table
            container.appendChild(stockquotes.showData());
        }
    };
}());